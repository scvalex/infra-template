{ config, lib, pkgs, nixpkgs, ... }:
with lib;
{
  config = {
    nix = {
      extraOptions = ''
        experimental-features = nix-command flakes
      '';
      registry.nixpkgs.flake = nixpkgs;
      gc = {
        automatic = true;
        options = "--delete-old --delete-older-than 30d";
      };
    };
  };
}
