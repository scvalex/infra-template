default:
	just --choose

# build all machines locally
build:
	nice colmena build --keep-result

# push closures, but don't activate
push:
	colmena apply --keep-result push

# deploy and activate without reboot
deploy-now $target:
	echo "Deploying live to $target"
	colmena apply --on "$target" --keep-result

# deploy for next boot and then reboot
deploy-reboot $target:
	echo "Deploying to $target (will reboot)"
	colmena apply --on "$target" --keep-result boot
	colmena exec --on "$target" -- systemctl reboot
