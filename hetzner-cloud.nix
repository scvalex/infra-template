{ config, lib, ... }:
with lib;
{
  imports = [
    ./networking.nix
  ];

  # TODO Make the IPv6 address and gateway configurable.
  config = {
    boot.initrd.availableKernelModules = [
      "ata_piix"
      "sd_mod"
      "sr_mod"
      "virtio"
      "virtio_net"
      "virtio_pci"
      "virtio_scsi"
    ];
    boot.initrd.kernelModules = [ "virtio_gpu" ];
    boot.kernelParams = [
      "console=tty"
    ];

    # Without the following, DHCP is used to configure the network
    # which is enough for IPv4, but not IPv6.
    systemd.network.enable = true;
    systemd.network.networks."10-wan" = {
      matchConfig.Name = config.networking.publicInterface;
      linkConfig.RequiredForOnline = "routable";
      networkConfig = {
        DHCP = "ipv4";
        # IPv6AcceptRA = true;
      };
      # Hetzner doesn't auto-configure IPv6 over DHCP.
      address = [ "2a01:4f8:c0c:eea4::1/64" ];
      routes = [
        { routeConfig.Gateway = "fe80::1"; }
      ];
    };
  };
}
