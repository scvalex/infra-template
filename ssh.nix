{ ... }: {
  config = {
    users.users.root.openssh.authorizedKeys.keys = import ./authorized-keys.nix;
    services.openssh = {
      enable = true;
      settings = {
        PermitRootLogin = "prohibit-password";
        PasswordAuthentication = false;
      };
    };
    # TODO OpenSSH RCE: Remove this once
    # https://github.com/NixOS/nixpkgs/pull/323753 gets merged.
    services.openssh.settings.LoginGraceTime = 0;
  };
}
