{ config, lib, pkgs, ... }:
with lib;
let
  cfg = config.services.ghost-blog;
in
{
  options = {
    services.ghost-blog = {
      enable = mkEnableOption "ghost blog";

      acmeEmailAddress = mkOption {
        type = types.str;
        example = "admin@example.com";
        description = lib.mdDoc "The email address to give to Let's Encrypt.";
      };

      url = mkOption {
        type = types.str;
        example = "example.com";
        description = lib.mdDoc ''
          The URL the blog will be hosted at.  Do not include http:// or https:// here.
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    # If you didn't create the /var/lib/mysql dataset in ZFS with
    # disko, you can do so with this command:
    #
    # zfs create zroot/mysql -o mountpoint=/var/lib/mysql
    #
    services.mysql = {
      enable = true;
      package = pkgs.mysql80;
      dataDir = "/var/lib/mysql";
      ensureDatabases = [ "ghost" ];
      ensureUsers = [
        {
          name = "ghost";
          ensurePermissions = { "ghost.*" = "ALL PRIVILEGES"; };
        }
      ];
    };

    # Ghost needs the db user to be password-authenticated, so on the
    # server, run:
    #
    # # sudo mysql
    # mysql> ALTER USER 'ghost'@'localhost' IDENTIFIED WITH 'mysql_native_password' BY '<password from ./secrets/mysql-password>';
    #
    # If you didn't create the /var/lib/ghost dataset in ZFS with
    # disko, you can do so with this command:
    #
    # zfs create zroot/ghost -o mountpoint=/var/lib/ghost
    #
    # After Ghost is up, you'll need to go to the /ghost URL to create
    # an admin account:
    #
    # https://example.com/ghost
    virtualisation.oci-containers = {
      backend = "podman";
      containers.ghost = {
        # https://hub.docker.com/_/ghost
        image = "docker.io/library/ghost:5.94";
        volumes = [
          "/var/lib/ghost/content:/var/lib/ghost/content"
        ];
        # https://ghost.org/docs/config/#configuration-options
        environment = {
          "database__client" = "mysql";
          "database__connection__host" = "localhost";
          "database__connection__user" = "ghost";
          "database__connection__password" = removeSuffix "\n" (readFile (./secrets + "/mysql-password"));
          "database__connection__database" = "ghost";
          "sodoSearch__url" = "/deps/ghost-js/sodo-search/umd/sodo-search.min.js";
          "sodoSearch__styles" = "/deps/ghost-js/sodo-search/umd/main.css";
          "portal__url" = "/deps/ghost-js/portal/umd/portal.min.js";
          "comments__url" = "/deps/ghost-js/comments-ui/umd/comments-ui.min.js";
          "comments__styles" = "false";
          "url" = "https://${cfg.url}";
        };
        extraOptions = [
          "--network=host"
        ];
        autoStart = true;
      };
    };
    system.activationScripts = {
      create-pihole-var = ''
        mkdir -p /var/lib/ghost/content
      '';
    };

    # Nginx reverse proxy
    #
    # Don't forget to also open these ports in the console of your
    # cloud provider.
    networking.firewall.allowedTCPPorts = [ 80 443 ];
    services.nginx = {
      enable = true;
      clientMaxBodySize = "40M";
      recommendedProxySettings = true;
      virtualHosts."${cfg.url}" = {
        forceSSL = true;
        enableACME = true;
        locations."/" = {
          proxyPass = "http://127.0.0.1:2368";
          proxyWebsockets = true;
        };
        locations."/deps" = {
          root = pkgs.linkFarm "deps" [
            { name = "mathjax"; path = "${pkgs.nodePackages.mathjax}/lib/node_modules/mathjax"; }
            {
              name = "ghost-js";
              path = pkgs.buildNpmPackage {
                pname = "ghost-js";
                src = ./ghost-js;
                version = "5.94";
                npmDepsHash = "sha256-fFg9c3G8k1N40q/Pq0HkdCGPVbI9EVMTkZu3Q51S+nw=";
                NODE_OPTIONS = "--openssl-legacy-provider";
                installPhase = ''
                  mkdir -p $out
                  cp -r ./comments-ui/ $out/
                  cp -r ./portal/ $out/
                  cp -r ./sodo-search/ $out/
                '';
              };
            }
          ];
          extraConfig = ''
            rewrite ^/deps(/.*)$ $1 break;
          '';
        };
        locations."/robots.txt" = {
          root = pkgs.stdenv.mkDerivation {
            name = "ai-robots-txt";
            src = pkgs.fetchFromGitHub {
              owner = "ai-robots-txt";
              repo = "ai.robots.txt";
              rev = "v1.16";
              hash = "sha256-7hqFyGYxKL8nMqCcNEtiZqqWAdv781OyaGnsJPONx3A=";
            };
            installPhase = ''
              mkdir -p $out
              cat robots.txt > $out/robots.txt
              echo >> $out/robots.txt
              cat ${./robots.txt} >> $out/robots.txt
              chmod 0400 $out/robots.txt
            '';
          };
        };
      };
    };
    security.acme = {
      acceptTerms = true;
      defaults.email = cfg.acmeEmailAddress;
    };

    # Remote backups of the zroot/mysql and zroot/ghost datasets.
    #
    # You will need to manually give the right ZFS permissions to the
    # syncoid user:
    #
    # zfs allow syncoid send,hold zroot/mysql
    # zfs allow syncoid send,hold zroot/ghost
    services.sanoid = {
      enable = true;
      templates.prod-backup = {
        hourly = 72;
        daily = 30;
        monthly = 12;
        autosnap = true;
        autoprune = true;
      };
      datasets = {
        "zroot/ghost".useTemplate = [ "prod-backup" ];
        "zroot/mysql".useTemplate = [ "prod-backup" ];
      };
    };
    users.users.syncoid = {
      # This is only needed because we don't enable `services.syncoid`
      # on this host.  That's only enabled on the hosts that *pull*
      # backup to them.
      group = "syncoid";
      isSystemUser = true;
      home = "/var/lib/syncoid";
      createHome = false;
      # ssh-keygen -t ed25519 -C syncoid
      openssh.authorizedKeys.keyFiles = [ ./secrets/syncoid.pub ];
      useDefaultShell = true;
    };
    users.groups.syncoid = { };
  };
}
