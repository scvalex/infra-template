{ config, lib, ... }:
with lib;
{
  imports = [
    ./networking.nix
  ];

  config = {
    fileSystems = {
      "/" = {
        device = "zroot/nixos";
        fsType = "zfs";
      };
      "/boot-1" = {
        device = "/dev/disk/by-partlabel/BOOT-1";
        fsType = "vfat";
        options = [ "nofail" ];

      };
    };

    boot = {
      supportedFilesystems = [ "zfs" ];
      loader = {
        systemd-boot.enable = false;
        grub = {
          # no need to set devices, disko will add all devices that have a EF02 partition to the list already
          devices = lib.mkForce [ ];
          efiSupport = true;
          efiInstallAsRemovable = true;
          mirroredBoots = [
            { path = "/boot-1"; devices = [ "nodev" ]; }
          ];
          copyKernels = true;
        };
      };
      kernelParams = [
        # See <https://www.kernel.org/doc/Documentation/filesystems/nfs/nfsroot.txt> for docs on this
        # ip=<client-ip>:<server-ip>:<gw-ip>:<netmask>:<hostname>:<device>:<autoconf>:<dns0-ip>:<dns1-ip>:<ntp0-ip>
        # The server ip refers to the NFS server -- we don't need it.
        "ip=::::${config.networking.hostName}-initrd:${config.networking.publicInterface}:dhcp:"
      ];
      initrd.network = {
        enable = true;
        ssh = {
          enable = true;
          port = 2222;
          # hostKeys paths must be unquoted strings, otherwise you'll run into issues
          # with boot.initrd.secrets the keys are copied to initrd from the path specified;
          # multiple keys can be set you can generate any number of host keys using
          # 'ssh-keygen -t ed25519 -N "" -f /boot-1/initrd-ssh-key'
          hostKeys = [
            /boot-1/initrd-ssh-key
          ];
          # public ssh key used for login
          authorizedKeys = import ./authorized-keys.nix;
        };
        # this will automatically load the zfs password prompt on login
        # and kill the other prompt so boot can continue
        postCommands = ''
          cat <<EOG > /root/.profile
          if pgrep -x "zfs" > /dev/null
          then
            zpool import -f -a || true
            zfs load-key -a
            killall zfs
          else
            echo "zfs not running -- maybe the pool is taking some time to load for some unforseen reason."
          fi
          EOG
        '';
      };
    };

    disko.devices = {
      disk = {
        x = {
          type = "disk";
          device = "/dev/sda";
          content = {
            type = "gpt";
            partitions = {
              # TODO Going forward, add the boot partition (I don't
              # know how it was booting without it) and get rid of the
              # mirrored boots thing.
              #
              # boot = {
              #   name = "boot";
              #   size = "1M";
              #   type = "EF02";
              # };
              ESP = {
                size = "512M";
                type = "EF00";
                label = "BOOT-1";
                content = {
                  type = "filesystem";
                  format = "vfat";
                  mountpoint = "/boot-1";
                };
              };
              zfs = {
                size = "100%";
                content = {
                  type = "zfs";
                  pool = "zroot";
                };
              };
            };
          };
          postCreateHook = ''
            mkdir -p /boot-1
            mount /dev/disk/by-partlabel/BOOT-1 /boot-1
            ssh-keygen -t ed25519 -N "" -f /boot-1/initrd-ssh-key
            rm /boot-1/initrd-ssh-key.pub
            umount /boot-1
          '';
        };
      };
      zpool = {
        zroot = {
          type = "zpool";
          mode = "";
          options = {
            ashift = "12";
          };
          rootFsOptions = {
            mountpoint = "none";
            compression = "zstd";
            atime = "off";
            acltype = "posixacl";
            xattr = "sa";
            encryption = "aes-256-gcm";
            keyformat = "passphrase";
            keylocation = "file:///tmp/zpool-encryption.key";
          };
          mountpoint = null;
          postCreateHook = ''
            zfs set keylocation=prompt zroot;
          '';

          datasets = {
            nixos = {
              type = "zfs_fs";
              options.mountpoint = "legacy";
              mountpoint = "/";
            };
          };
        };
      };
    };
  };
}
