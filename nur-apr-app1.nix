{ modulesPath, config, lib, pkgs, ... }: {
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    (modulesPath + "/profiles/qemu-guest.nix")
    ./better-nix.nix
    ./ghost-blog.nix
    ./hetzner-cloud.nix
    ./networking.nix
    ./ssh.nix
    ./zfs-one-disk-encrypted.nix
    ./scvalex-user.nix
  ];

  deployment.targetHost = "157.90.166.239";
  system.stateVersion = "23.11";
  nixpkgs.system = "aarch64-linux";

  networking.hostName = "nur-apr-app1";
  networking.hostId = "dadadada"; # same for all VMs
  networking.publicInterface = "enp1s0";

  # See the package search for more packages:
  # https://search.nixos.org/packages?query=curl
  environment.systemPackages = with pkgs; [
    curl
    helix
  ];

  # See the option search for more options:
  # https://search.nixos.org/options?query=postgres

  services.ghost-blog = {
    enable = true;
    url = "forthinvesting.com";
    acmeEmailAddress = "ssl@abstractbinary.org";
  };
}
