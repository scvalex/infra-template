{ config, lib, ... }:
with lib;
{
  options = {
    networking.publicInterface = mkOption {
      type = types.str;
      description = "Public interface";
    };
  };
}
