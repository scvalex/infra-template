{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    colmena = {
      url = "github:zhaofengli/colmena/stable";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    nixos-anywhere = {
      url = "github:nix-community/nixos-anywhere";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nixos-stable.follows = "nixpkgs";
    };
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs =
    { self
    , nixpkgs
    , nixpkgs-unstable
    , flake-utils
    , nixos-anywhere
    , disko
    , colmena
    , ...
    }:
    ({
      colmena = {
        # The following configurations are used when remotely
        # re-configuring or upgrading a deployment.  The initial
        # deployment is special and is configured through the
        # `nixosConfiguration` section below.
        #
        # Upgrade: colmena apply --on nur-apr-app1
        # Common tasks: see the `justfile` in this repo
        # Unlock: ssh root@NNN.NNN.NNN.NNN -p 2222
        # Login: ssh root@NNN.NNN.NNN.NNN
        meta = {
          nixpkgs = import nixpkgs {
            system = "x86_64-linux";
          };
          specialArgs = {
            inherit nixpkgs;
          };
        };
        defaults = {
          imports = [
            disko.nixosModules.disko
          ];
        };

        nur-apr-app1 = import ./nur-apr-app1.nix;
        # nur-apr-app2 = import ./nur-apr-app2.nix;
        # nur-apr-app3 = import ./nur-apr-app3.nix;
        # ... more hosts for remote deployment ...
      };
      nixosConfigurations = {
        # The following configurations are only used during the
        # initial deployment.  Afterwards, the system configuration is
        # taken from the `colmena` section above.
        #
        # Deploy: nixos-anywhere --disk-encryption-keys /tmp/zpool-encryption.key ./secrets/zpool-encryption.key --flake .#nur-apr-app1 root@NNN.NNN.NNN.NNN
        # Unlock: ssh root@NNN.NNN.NNN.NNN -p 2222
        # Login: ssh root@NNN.NNN.NNN.NNN
        #
        nur-apr-app1 = nixpkgs.lib.nixosSystem {
          system = "aarch64-linux";
          modules = [
            disko.nixosModules.disko
            colmena.nixosModules.deploymentOptions
            (import ./nur-apr-app1.nix)
          ];
        };
        # nur-apr-app2 = nixpkgs.lib.nixosSystem {
        #   ...
        # }
        # ... more hosts for initial deployment ...
      };
    }
    // flake-utils.lib.eachDefaultSystem
      (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          config.allowUnfree = true;
        };
      in
      {
        devShell = with pkgs; mkShell {
          buildInputs = [
            colmena.packages."${system}".colmena
            difftastic
            just
            nixos-anywhere.packages."${system}".default
            git-crypt
            nodejs
          ];
          GIT_EXTERNAL_DIFF = "${difftastic}/bin/difft";
        };
      }));
}
