{ config, lib, pkgs, ... }:

with lib;
let
in
{
  users.groups = {
    scvalex = { gid = 1001; };
  };

  users.users = {
    scvalex = {
      uid = 1001;
      group = "scvalex";
      extraGroups = [ "wheel" ];
      isNormalUser = true;
      # openssh.authorizedKeys.keyFiles = [ ./secrets/ssh/infra.pub ];
      hashedPassword = "$y$j9T$as02T0n2vUr3rKZG9/3OK0$061EZ6AyMjftBKVRydZTMCrruAuMjkM7uDUhrzORJF9";
    };
  };

  # users.users.root.openssh.authorizedKeys.keyFiles = [ ./secrets/scvalex.pub ];
}
